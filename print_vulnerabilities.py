import json
from tabulate import tabulate
import sys,os

with open("./msec_vulnerabilities.json") as f:
        data = json.load(f)

IMAGE_NAME = os.environ.get('IMAGE_NAME', None)

if not IMAGE_NAME:
    print("Image name variable is not exported")
    sys.exit(1)

if 'status' in data:
    if data['status'] == 'success':
        print("No Vulnerabilities Found!!")
        sys.exit()
    else:
        if 'number_of_vulnerabilities' in data:
            print(f"\nTotal vulnerabilities reported for the image {IMAGE_NAME} are:\t {data.get('number_of_vulnerabilities', None)}\n")

        headers = ['CVE-ID', 'Severity', 'Vulnerability Package', 'Fix Available', 'Threat Score', 'URL']
        issues = []
        vulnerabilities_count = 0
        severity_count = {"Critical":0, "High":0, "Medium":0, "Low":0, "Unknown":0}
        action_count = {"Stop":0, "Warn":0}
        if 'vulnerabilities_list' in data:
            for issue in data['vulnerabilities_list']:
                vulnerabilities_count+=1

                if vulnerabilities_count < 10:
                    issues.append( [ issue['name'], issue.get('severity', 'Unknown'), issue.get('feature_name',''), issue.get('fixed_by', None), issue.get('threat_score','Not Available'), issue.get('link', '')]  )
                
                if issue.get('action', None) != None:
                    action_count[issue.get('action')] = action_count.get(issue.get('action'), 0) + 1

                if issue.get('severity', None) != None:
                    severity_count[issue.get('severity')] = severity_count.get(issue.get('severity'),0 ) + 1
        print("\n\nVulnerability Severity Count for the Image:\n")
        print(tabulate([severity_count.values()], headers=severity_count.keys(), tablefmt='fancy_grid'))
        print("\n\n\n\nImage Policy Actions:\n")
        print(tabulate([action_count.values()], headers=action_count.keys(), tablefmt='fancy_grid'))
        print("\n\nSome vulnerabilties found in this image are:\n\n")
        print(tabulate(issues, headers=headers, tablefmt='fancy_grid'))
        print("\n\nPlease check microsec dashboard for detailed analysis of all vulnerabilities..")
